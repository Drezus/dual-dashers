﻿using UnityEngine;

public class MissileDisappear : MonoBehaviour
{
    public void Start()
    {
        Destroy(gameObject, 5);
    }
}