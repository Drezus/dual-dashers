using UnityEngine;

public class MoveEnemyAI : MonoBehaviour
{
    public  WheelCollider frontLeftWheel;
    public  WheelCollider frontRightWheel;

    public BoxCollider myOilSensor;
    public BoxCollider myMissileSensor;

    private bool useItem = false;

    public Transform kartDir;

    public float          speed          = 15;
	public  float         gravity        = 10;
	public  float         maxSpeedChange = 15;
	private  float        rotSpeed        = 0.6f;
    private float         steerSpeed     = 1;
	private bool          grounded       = false;
    private bool          canControl     = true;

    private float orbSpeed = 0;
    public float jumpStrenght = 4.3f;

    private bool canDrift = false;

    private bool overWater = false;
    private bool overOil = false;
    private bool overIce = false;
    private bool overDirt = false;

    public bool blinking = false;
    private float blink;
    public float blinkTime = 4;
    private float blinkInterval;

    private string currentItem = null;

    public Material enemyDef;
    public Material tiresDef;
    public Material enemyGhost;
    public Material tiresGhost;

    public GameObject objOil;
    public Transform OilSpawn;

    private bool hasItem = false;
    private bool hasMiniTurbo = false;

    private float sparkTimer = 0;
    private float maxSparkTimer = 1.5f;

    private float ghostTimer = 0;
    private float maxGhostTimer = 5f;
    private bool isGhost = false;

    public GameObject missileBlast;
    public Rigidbody missile;
    public Transform kartNozzle;

    public void Start()
    {
        foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
        {
            spark.renderer.enabled = false;
        }
        foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("enemybluesparks"))
        {
            bluespark.renderer.enabled = false;
        }

    }

    public void FixedUpdate()
	{
		float vertical   = 1;
		float horizontal = 0;

        kartDir.position = transform.position;
        if (!overIce && !overWater && !overOil && canControl)
        {
            kartDir.rotation = transform.rotation;
        }
		
		if (grounded)
		{
            if (canControl)
            {
                Vector3 targetVelocity = Vector3.zero;
                if (!overDirt)
                {
                    targetVelocity = kartDir.transform.forward * (speed + (orbSpeed / 6)) * vertical;
                }
                else
                {
                    targetVelocity = (kartDir.transform.forward * (speed + (orbSpeed / 6)) * vertical) / 2;
                }
                Vector3 velocity = rigidbody.velocity;
                Vector3 velocityChange = (targetVelocity - velocity);
                velocityChange.x = Mathf.Clamp(velocityChange.x, -maxSpeedChange, maxSpeedChange);
                velocityChange.z = Mathf.Clamp(velocityChange.z, -maxSpeedChange, maxSpeedChange);
                velocityChange.y = 0;

                rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

                if (vertical != 0)
                {
                    float sign = Mathf.Sign(vertical);
                    transform.Rotate(0, sign * rotSpeed * horizontal, 0);
                }
            }
            else
            {
                vertical = 0;
                horizontal = 0;
            }

            if (vertical == 1 && !overIce && !overWater && !overOil && !overDirt && canControl)
            {
                if (horizontal > 0.5 || horizontal < -0.5)
                {
                    canDrift = true;
                }
                else
                {
                    canDrift = false;
                    sparkTimer = 0;
                   
                    foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
                    {
                        spark.renderer.enabled = false;
                    }
                    foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("enemybluesparks"))
                    {
                        bluespark.renderer.enabled = false;
                    }
                    if (hasMiniTurbo)
                    {
                        UseMiniTurbo();
                    }
                }
            }
            else
            {
                sparkTimer = 0;
                canDrift = false;
                
                foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
                {
                    spark.renderer.enabled = false;
                }
                foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("enemybluesparks"))
                {
                    bluespark.renderer.enabled = false;
                }
                if (hasMiniTurbo)
                {
                    UseMiniTurbo();
                }
                hasMiniTurbo = false;
            }
			
		}

        rigidbody.AddForce(new Vector3(0, -gravity * rigidbody.mass, 0));

        CheckMissileDist();
        CheckOilDist();

        useItem = false;
	}
	
	public void OnCollisionStay(Collision hit) 
	{
    	if (hit.gameObject.tag == "floor") grounded = true;    
	}

	public void OnCollisionExit(Collision hit)
	{
		if (hit.gameObject.tag == "floor") grounded = false;    	
	}

    public void Update()
    {
        //Mudar a for�a de RotSpeed pra driftar
        if (canDrift)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                //rotSpeed = 1.5f;
                if (!hasMiniTurbo)
                {
                    foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
                    {
                        spark.renderer.enabled = true;
                    }
                }
                else
                {
                    foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
                    {
                        spark.renderer.enabled = false;
                    }
                }
                SparkCountdown();
            }
            else
            {
                sparkTimer = 0;
                //rotSpeed = 0.6f;
                foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
                {
                    spark.renderer.enabled = false;
                }
                foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("enemybluesparks"))
                {
                    bluespark.renderer.enabled = false;
                }
            }
        }

        //Miniturbo
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            sparkTimer = 0;
            if (hasMiniTurbo)
            {
                UseMiniTurbo();
            }
            foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("enemybluesparks"))
            {
                bluespark.renderer.enabled = false;
            }
            foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
            {
                spark.renderer.enabled = false;
            }
        }

        //Verificar se est� sobre a �gua e travar o offset se estiver
        if (overWater)
        {
            if (kartDir.rotation != transform.rotation && !blinking)
            {
                //Remover controle se escorregar na �gua (offset diferente do original)
                SpinOut();
            }
        }

        //Verificar se est� sobre o �leo. �leos s�o impiedosos e n�o te d�o chance para falhas, MWHAHAHAHAHA.
        if (overOil)
        {
            if (!blinking)
            {
                //Remover controle se PASSAR sobre o �leo
                SpinOut();
                overOil = false;
            }
        }

        //Restaurar controle
        if (!animation.IsPlaying("spin") && !canControl)
        {
            canControl = true;
            transform.rotation = kartDir.rotation;
        }

        #region Blink Stuff
        //Tempo de Prote��o contra armas/�gua (+ Blink System)
        if (blinking)
        {
            blink += Time.deltaTime;

            blinkInterval += Time.deltaTime;

            if (blinkInterval <= 0.25)
            {
                gameObject.renderer.enabled = false;
                foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("enemywheel"))
                {
                    wheel.renderer.enabled = false;
                }

            }
            else if (blinkInterval > 0.25)
            {
                gameObject.renderer.enabled = true;
                foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("enemywheel"))
                {
                    wheel.renderer.enabled = true;
                }
            }

            if (blinkInterval >= 0.5)
            {
                blinkInterval = 0;
            }

            if (blink >= blinkTime)
            {
                blink = 0;
                blinking = false;
                blinkInterval = 0;
                gameObject.renderer.enabled = true;
            }
        }
        #endregion

        //Itens!
        if (useItem && hasItem && grounded && canControl)
        {
            if (currentItem == "turbo")
            {
                UseTurbo();
            }
            if (currentItem == "missile")
            {
                ShootMissile();
            }
            if (currentItem == "ghost")
            {
                UseGhost();
            }
            if (currentItem == "oil")
            {
                DropOil();
            }
            EraseItem();
        }

        //Ghost Update Things
        if (isGhost)
        {
            ghostTimer += Time.deltaTime;

            if (ghostTimer >= maxGhostTimer)
            {
                ghostTimer = 0;
                gameObject.renderer.material = enemyDef;
                foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("enemywheel"))
                {
                    wheel.renderer.material = tiresDef;
                }
                isGhost = false;
                return;
            }
        }

        //CHEATS STUFF
        if (Input.GetKey(KeyCode.Alpha5))
        {
            GotTurbo();
        }
        if (Input.GetKey(KeyCode.Alpha6))
        {
            GotMissile();
        }
        if (Input.GetKey(KeyCode.Alpha7))
        {
            GotGhost();
        }
        if (Input.GetKey(KeyCode.Alpha8))
        {
            GotOil();
        }
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "water" && !blinking && !isGhost) overWater = true;
        if (hit.gameObject.tag == "oil" && !blinking && !isGhost) { overOil = true; Destroy(hit.gameObject); }
        if (hit.gameObject.tag == "ice" && !isGhost) overIce = true;
        if (hit.gameObject.tag == "dirt" && !isGhost) overDirt = true;
        if (hit.gameObject.tag == "jump") Jump();
        if (hit.gameObject.tag == "lava") Pit();
        if (hit.gameObject.tag == "pit") Pit();
        if (hit.gameObject.tag == "orb") { hit.gameObject.SendMessage("Disappear", SendMessageOptions.DontRequireReceiver); AddOrbSpeed(); }
        if (hit.gameObject.tag == "itembox") { hit.gameObject.SendMessage("Disappear", SendMessageOptions.DontRequireReceiver); GetItem(); }
        if (hit.gameObject.tag == "ghostChance" && currentItem == "ghost") { UseItem(); }
        if (hit.gameObject.tag == "turboChance" && currentItem == "turbo") { UseItem(); }
        if (hit.gameObject.tag == "anyChance" && hasItem == true) { UseItem(); }
    }

    public void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "water") overWater = false;
        if (hit.gameObject.tag == "oil") overOil = false;
        if (hit.gameObject.tag == "ice") overIce = false;
        if (hit.gameObject.tag == "dirt") overDirt = false;
    }

    public void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.tag == "missile" && !blinking && !isGhost) { SpinOut(); Instantiate(missileBlast, hit.gameObject.transform.position, hit.gameObject.transform.rotation); Destroy(hit.gameObject); }
    }

    public void Jump()
    {
        rigidbody.AddForce(Vector3.up * jumpStrenght * 100);
        return;
    }

    public void Pit()
    {
        //gameObject.GetComponent<Collider>().enabled = true;
        gameObject.transform.position = GameObject.Find("RESTOREPOINT").transform.position;
        gameObject.transform.rotation = GameObject.Find("RESTOREPOINT").transform.rotation;
        //moveDir = Vector3.zero;
        blinking = true;
        GetComponent<WaypointAI>().currentWayPoint = 24;
        SubtractOrbSpeed();
        return;
    }

    public void SpinOut()
    {
        animation.Play("spin");
        canControl = false;
        blinking = true;
        SubtractOrbSpeed();
    }

    public void AddOrbSpeed()
    {
        if (orbSpeed < 10)
        {
            orbSpeed += 0.5f;
        }
    }

    public void SubtractOrbSpeed()
    {
        if (orbSpeed > 1)
        {
            orbSpeed -= Mathf.Floor(orbSpeed / 2);
        }
    }

    public void GetItem()
    {
        if (!hasItem)
        {
            int r = (int)UnityEngine.Random.Range(1, 5);
            switch (r)
            {
                case 1:
                    GotTurbo();
                    break;
                case 2:
                    GotMissile();
                    break;
                case 3:
                    GotGhost();
                    break;
                case 4:
                    GotOil();
                    break;
                default:
                    Debug.LogError("ERROR: Couldn't assign an Item to the current random number range");
                    break;
            }
        }
    }

    public void GotTurbo()
    {
        currentItem = "oil";
        //currentItem = "turbo";
        hasItem = true;
    }

    public void GotMissile()
    {
        currentItem = "oil";
        //currentItem = "missile";
        hasItem = true;
    }

    public void GotGhost()
    {
        currentItem = "oil";
        //currentItem = "ghost";
        hasItem = true;
    }

    public void GotOil()
    {
        currentItem = "oil";
        //currentItem = "oil";
        hasItem = true;
    }

    public void EraseItem()
    {
        currentItem = null;
        hasItem = false;
    }


    public void UseTurbo()
    {
        rigidbody.AddForce(transform.forward * 8000);
        return;
    }

    public void UseMiniTurbo()
    {
        if (grounded)
        {
            rigidbody.AddForce(transform.forward * 5800);
        }
        hasMiniTurbo = false;
        return;
    }

    public void ShootMissile()
    {
        Rigidbody mss = Instantiate(missile, kartNozzle.transform.position, kartNozzle.transform.rotation) as Rigidbody;
        mss.AddForce(transform.forward * 2500);
        return;
    }

    public void UseGhost()
    {
        gameObject.renderer.material = enemyGhost;
        foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("enemywheel"))
        {
            wheel.renderer.material = tiresGhost;
        }
        overWater = false;
        overDirt = false;
        overIce = false;
        overOil = false;
        isGhost = true;
    }

    public void DropOil()
    {
        Instantiate(objOil, OilSpawn.transform.position, OilSpawn.transform.rotation);
        return;
    }


    public void SparkCountdown()
    {
        sparkTimer += Time.deltaTime;

        if (sparkTimer >= maxSparkTimer)
        {
            sparkTimer = 0;
            foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("enemybluesparks"))
            {
                bluespark.renderer.enabled = true;
            }
            foreach (GameObject spark in GameObject.FindGameObjectsWithTag("enemysparks"))
            {
                spark.renderer.enabled = false;
            }
            hasMiniTurbo = true;
            return;
        }  
    }

    public void UseItem()
    {
        useItem = true;
    }

    public void CheckOilDist()
    {
        RaycastHit hit;
        Ray backCarDist = new Ray(transform.position, transform.forward * -60);
        if (Physics.Raycast(backCarDist, out hit, 60))
        {
            print(hit.collider.tag);

            if (hit.collider.tag == "Player")
            {
               if (currentItem == "oil")
                {
                    UseItem();
                }
            }
        }
        Debug.DrawRay(transform.position, transform.forward * -60, Color.green);
        
        return;
    }

    public void CheckMissileDist()
    {
        RaycastHit hit2;
        Ray frontCarDist = new Ray(transform.position, transform.forward * 300);
        if (Physics.Raycast(frontCarDist, out hit2, 300))
        {
            print(hit2.collider.tag);

            if (hit2.collider.tag == "Player")
            {
                if (currentItem == "missile")
                {
                    UseItem();
                }
            }
        }
        Debug.DrawRay(transform.position, transform.forward * 300, Color.red);
        return;
    }
}