﻿using UnityEngine;

public class TimedRespawn : MonoBehaviour
{
    public void Respawn()
    {
        gameObject.SetActive(true);
    }

    public void Disappear()
    {
        gameObject.SetActive(false);
        Invoke("Respawn", 5);
    }
}