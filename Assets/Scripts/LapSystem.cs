﻿using UnityEngine;

public class LapSystem : MonoBehaviour
{
    public int lap = 1;
    public GUIText lapText;
    private int currentChk = 0;
    public GameObject[] aChks;

    public void Start()
    {
        lap = 1;
    }

    public void Update()
    {
        lapText.text = lap.ToString();
    }

    public void OnTriggerEnter(Collider hit)
    {
        for (int totalChks = 0; totalChks <= aChks.Length; totalChks++)
        {
            if (hit.gameObject == aChks[currentChk])
            {
               currentChk++;
               if (currentChk == aChks.Length)
               {
                   currentChk = 0; 
                   lap++;
                   if (lap == 4)
                   {
                       SendMessage("GameWon", SendMessageOptions.DontRequireReceiver);
                   }
               }
            }
        }
    }
}