using UnityEngine;

public class MoveRabit : MonoBehaviour
{
    public  WheelCollider frontLeftWheel;
    public  WheelCollider frontRightWheel;

    public Transform kartDir;

	public  float         speed          = 15;
	public  float         gravity        = 10;
	public  float         maxSpeedChange = 15;
	private  float        rotSpeed        = 0.6f;
    private float         steerSpeed     = 1;
	private bool          grounded       = false;
    private bool          canControl     = true;

    private float orbSpeed = 0;
    public float jumpStrenght = 4.3f;

    private bool canDrift = false;

    private bool overWater = false;
    private bool overOil = false;
    private bool overIce = false;
    private bool overDirt = false;

    public bool blinking = false;
    private float blink;
    public float blinkTime = 4;
    private float blinkInterval;

    public GUIText numOrbs;

    private string currentItem = null;
    public GUITexture itemHUD;
    public Texture text_empty;
    public Texture text_turbo;
    public Texture text_missile;
    public Texture text_ghost;
    public Texture text_oil;

    public Material playerDef;
    public Material tiresDef;
    public Material playerGhost;
    public Material tiresGhost;

    public GameObject objOil;
    public Transform OilSpawn;

    private bool hasItem = false;
    private bool hasMiniTurbo = false;

    private float sparkTimer = 0;
    private float maxSparkTimer = 1.5f;

    private float ghostTimer = 0;
    private float maxGhostTimer = 5f;
    private bool isGhost = false;

    public GameObject missileBlast;
    public Rigidbody missile;
    public Transform kartNozzle;

    public AudioSource speedOrbSound;
    public float orbPitchLevel;

    private bool gameEnded = false;
    public AudioSource musicEmitter;
    public GUIText winText;
    public GUIText loseText;
    public AudioClip winTheme;
    public AudioClip loseTheme;
    public AudioClip ghostSound;
    public AudioClip turboSound;

    public void Start()
    {
        foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
        {
            spark.renderer.enabled = false;
        }
        foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("bluesparks"))
        {
            bluespark.renderer.enabled = false;
        }

        winText.gameObject.SetActive(false);
        loseText.gameObject.SetActive(false);
    }

    public void FixedUpdate()
	{
		float vertical   = Input.GetAxis("Vertical");
		float horizontal = Input.GetAxis("Horizontal");

        kartDir.position = transform.position;
        if (!overIce && !overWater && !overOil && canControl)
        {
            kartDir.rotation = transform.rotation;
        }
		
		if (grounded)
		{
            if(canControl)
            {
                Vector3 targetVelocity = Vector3.zero;
                if (!overDirt)
                {
                    targetVelocity = kartDir.transform.forward * (speed + (orbSpeed / 6)) * vertical;
                }
                else
                {
                    targetVelocity = (kartDir.transform.forward * (speed + (orbSpeed / 6)) * vertical) / 2;
                }
	            Vector3 velocity       = rigidbody.velocity;
	            Vector3 velocityChange = (targetVelocity - velocity);
	            velocityChange.x       = Mathf.Clamp(velocityChange.x, -maxSpeedChange, maxSpeedChange);
	            velocityChange.z       = Mathf.Clamp(velocityChange.z, -maxSpeedChange, maxSpeedChange);
	            velocityChange.y       = 0;
			
			    rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
			
			    if (vertical != 0)
			    {
				    float sign = Mathf.Sign(vertical);
				    transform.Rotate(0, sign * rotSpeed * horizontal, 0);
			    }
            }

            if (vertical == 1 && !overIce && !overWater && !overOil && !overDirt && canControl)
            {
                if (horizontal > 0.5 || horizontal < -0.5)
                {
                    canDrift = true;
                }
                else
                {
                    canDrift = false;
                    //sparkTimer = 0;
                    //rotSpeed = 0.4f;
                    //foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
                    //{
                    //    spark.renderer.enabled = false;
                    //}
                    //foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("bluesparks"))
                    //{
                    //    bluespark.renderer.enabled = false;
                    //}
                    //if (hasMiniTurbo)
                    //{
                    //    UseMiniTurbo();
                    //}
                }
            }
            else
            {
                sparkTimer = 0;
                canDrift = false;
                rotSpeed = 0.6f;
                foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
                {
                    spark.renderer.enabled = false;
                }
                foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("bluesparks"))
                {
                    bluespark.renderer.enabled = false;
                }
                //if (hasMiniTurbo)
                //{
                //    UseMiniTurbo();
                //}
                hasMiniTurbo = false;
            }
			
		}

        rigidbody.AddForce(new Vector3(0, -gravity * rigidbody.mass, 0));

        frontLeftWheel.steerAngle  = steerSpeed * horizontal;
        frontRightWheel.steerAngle = steerSpeed * horizontal;
	}
	
	public void OnCollisionStay(Collision hit) 
	{
    	if (hit.gameObject.tag == "floor") grounded = true;    
	}

	public void OnCollisionExit(Collision hit)
	{
		if (hit.gameObject.tag == "floor") grounded = false;    	
	}

    public void Update()
    {
        //Mudar a for�a de RotSpeed pra driftar
        if (canDrift)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                rotSpeed = 1.5f;
                if (!hasMiniTurbo)
                {
                    foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
                    {
                        spark.renderer.enabled = true;
                    }
                }
                else
                {
                    foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
                    {
                        spark.renderer.enabled = false;
                    }
                }
                SparkCountdown();
            }
            else
            {
                sparkTimer = 0;
                rotSpeed = 0.6f;
                foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
                {
                    spark.renderer.enabled = false;
                }
                foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("bluesparks"))
                {
                    bluespark.renderer.enabled = false;
                }
            }
        }

        //Miniturbo
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            sparkTimer = 0;
            if (hasMiniTurbo)
            {
                UseMiniTurbo();
            }
            foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("bluesparks"))
            {
                bluespark.renderer.enabled = false;
            }
            foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
            {
                spark.renderer.enabled = false;
            }
        }

        //Verificar se est� sobre a �gua e travar o offset se estiver
        if (overWater)
        {
            if (kartDir.rotation != transform.rotation && !blinking)
            {
                //Remover controle se escorregar na �gua (offset diferente do original)
                SpinOut();
            }
        }

        //Verificar se est� sobre o �leo. �leos s�o impiedosos e n�o te d�o chance para falhas, MWHAHAHAHAHA.
        if (overOil)
        {
            if (!blinking)
            {
                //Remover controle se PASSAR sobre o �leo
                SpinOut();
                overOil = false;
            }
        }

        //Restaurar controle
        if (!animation.IsPlaying("spin") && !canControl && !gameEnded)
        {
            canControl = true;
            transform.rotation = kartDir.rotation;
        }

        #region Blink Stuff
        //Tempo de Prote��o contra armas/�gua (+ Blink System)
        if (blinking)
        {
            blink += Time.deltaTime;

            blinkInterval += Time.deltaTime;

            if (blinkInterval <= 0.25)
            {
                gameObject.renderer.enabled = false;
                foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("wheel"))
                {
                    wheel.renderer.enabled = false;
                }

            }
            else if (blinkInterval > 0.25)
            {
                gameObject.renderer.enabled = true;
                foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("wheel"))
                {
                    wheel.renderer.enabled = true;
                }
            }

            if (blinkInterval >= 0.5)
            {
                blinkInterval = 0;
            }

            if (blink >= blinkTime)
            {
                blink = 0;
                blinking = false;
                blinkInterval = 0;
                gameObject.renderer.enabled = true;
            }
        }
        #endregion

        numOrbs.text = (orbSpeed).ToString();

        //Itens!
        if (Input.GetKeyDown(KeyCode.Space) && hasItem && grounded && canControl)
        {
            if (currentItem == "turbo")
            {
                UseTurbo();
            }
            if (currentItem == "missile")
            {
                ShootMissile();
            }
            if (currentItem == "ghost")
            {
                UseGhost();
            }
            if (currentItem == "oil")
            {
                DropOil();
            }
            EraseItem();
        }

        //Ghost Update Things
        if (isGhost)
        {
            ghostTimer += Time.deltaTime;

            if (ghostTimer >= maxGhostTimer)
            {
                ghostTimer = 0;
                gameObject.renderer.material = playerDef;
                foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("wheel"))
                {
                    wheel.renderer.material = tiresDef;
                }
                isGhost = false;
                return;
            }
        }

        //Orb Pitch Level
        {
            orbPitchLevel = (orbSpeed * 0.4f) + 1.5f;
            speedOrbSound.pitch = orbPitchLevel;
        }

        //CHEATS STUFF
        if (Input.GetKey(KeyCode.Alpha1))
        {
            GotTurbo();
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            GotMissile();
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            GotGhost();
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            GotOil();
        }
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            GameWon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            GameLost();
        }
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "water" && !blinking && !isGhost) overWater = true;
        if (hit.gameObject.tag == "oil" && !blinking && !isGhost) { overOil = true; Destroy(hit.gameObject); }
        if (hit.gameObject.tag == "ice" && !isGhost) overIce = true;
        if (hit.gameObject.tag == "dirt" && !isGhost) overDirt = true;
        if (hit.gameObject.tag == "jump") Jump();
        if (hit.gameObject.tag == "lava") Pit();
        if (hit.gameObject.tag == "pit") Pit();
        if (hit.gameObject.tag == "orb") { hit.gameObject.SendMessage("Disappear", SendMessageOptions.DontRequireReceiver); AddOrbSpeed(); speedOrbSound.Play(); }
        if (hit.gameObject.tag == "itembox") { hit.gameObject.SendMessage("Disappear", SendMessageOptions.DontRequireReceiver); GetItem(); }
    }

    public void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.tag == "water") overWater = false;
        if (hit.gameObject.tag == "oil") overOil = false;
        if (hit.gameObject.tag == "ice") overIce = false;
        if (hit.gameObject.tag == "dirt") overDirt = false;
    }

    public void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.tag == "missile" && !blinking && !isGhost) { SpinOut(); Instantiate(missileBlast, hit.gameObject.transform.position, hit.gameObject.transform.rotation); Destroy(hit.gameObject); }
    }

    public void Jump()
    {
        rigidbody.AddForce(Vector3.up * jumpStrenght * 100);
        return;
    }

    public void Pit()
    {
        //gameObject.GetComponent<Collider>().enabled = true;
        gameObject.transform.position = GameObject.Find("RESTOREPOINT").transform.position;
        gameObject.transform.rotation = GameObject.Find("RESTOREPOINT").transform.rotation;
        //moveDir = Vector3.zero;
        blinking = true;
        SubtractOrbSpeed();
        return;
    }

    public void SpinOut()
    {
        animation.Play("spin");
        canControl = false;
        blinking = true;
        SubtractOrbSpeed();
    }

    public void AddOrbSpeed()
    {
        if (orbSpeed < 10)
        {
            orbSpeed += 0.5f;
        }
    }

    public void SubtractOrbSpeed()
    {
        if (orbSpeed > 1)
        {
            orbSpeed -= Mathf.Floor(orbSpeed / 2);
        }
    }

    public void GetItem()
    {
        if (!hasItem)
        {
            int r = (int)UnityEngine.Random.Range(1, 5);
            switch (r)
            {
                case 1:
                    GotTurbo();
                    break;
                case 2:
                    GotMissile();
                    break;
                case 3:
                    GotGhost();
                    break;
                case 4:
                    GotOil();
                    break;
                default:
                    Debug.LogError("ERROR: Couldn't assign an Item to the current random number range");
                    break;
            }
        }
    }

    public void GotTurbo()
    {
        itemHUD.texture = text_turbo;
        currentItem = "turbo";
        hasItem = true;
        
    }

    public void GotMissile()
    {
        itemHUD.texture = text_missile;
        currentItem = "missile";
        hasItem = true;
    }

    public void GotGhost()
    {
        itemHUD.texture = text_ghost;
        currentItem = "ghost";
        hasItem = true;
    }

    public void GotOil()
    {
        itemHUD.texture = text_oil;
        currentItem = "oil";
        hasItem = true;
    }

    public void EraseItem()
    {
        itemHUD.texture = text_empty;
        currentItem = null;
        hasItem = false;
    }


    public void UseTurbo()
    {
        musicEmitter.PlayOneShot(turboSound);
        rigidbody.AddForce(transform.forward * 8000);
        return;
    }

    public void UseMiniTurbo()
    {
        musicEmitter.PlayOneShot(turboSound);
        if (grounded)
        {
            rigidbody.AddForce(transform.forward * 5800);
        }
        hasMiniTurbo = false;
        return;
    }

    public void ShootMissile()
    {
        Rigidbody mss = Instantiate(missile, kartNozzle.transform.position, kartNozzle.transform.rotation) as Rigidbody;
        mss.AddForce(transform.forward * 2500);
        return;
    }

    public void UseGhost()
    {
        musicEmitter.PlayOneShot(ghostSound);
        gameObject.renderer.material = playerGhost;
        foreach (GameObject wheel in GameObject.FindGameObjectsWithTag("wheel"))
        {
            wheel.renderer.material = tiresGhost;
        }
        overWater = false;
        overDirt = false;
        overIce = false;
        overOil = false;
        isGhost = true;
    }

    public void DropOil()
    {
        Instantiate(objOil, OilSpawn.transform.position, OilSpawn.transform.rotation);
        return;
    }


    public void SparkCountdown()
    {
        sparkTimer += Time.deltaTime;

        if (sparkTimer >= maxSparkTimer)
        {
            sparkTimer = 0;
            foreach (GameObject bluespark in GameObject.FindGameObjectsWithTag("bluesparks"))
            {
                bluespark.renderer.enabled = true;
            }
            foreach (GameObject spark in GameObject.FindGameObjectsWithTag("sparks"))
            {
                spark.renderer.enabled = false;
            }
            hasMiniTurbo = true;
            return;
        }  
    }

    public void GameWon()
    {
        gameEnded = true;
        canControl = false;
        winText.gameObject.SetActive(true);
        musicEmitter.Stop();
        musicEmitter.PlayOneShot(winTheme);

    }

    public void GameLost()
    {
        gameEnded = true;
        canControl = false;
        SpinOut();
        loseText.gameObject.SetActive(true);
        musicEmitter.Stop();
        musicEmitter.PlayOneShot(loseTheme);
    }
}

