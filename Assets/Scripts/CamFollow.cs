using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour
{
    public GameObject target;
    Vector3 initialOffset;
    public float rotDamping = 1;
    public float distDamping = 1;
    private bool dragAround = false;

    void Start()
    {
        initialOffset = target.transform.position - transform.position;
    }

    void LateUpdate()
    {
        Vector3 targetDist = target.transform.position - transform.position;
        if (targetDist.magnitude >= initialOffset.magnitude)
        {
            dragAround = true;
        }
        else if (targetDist.magnitude < initialOffset.magnitude)
        {
            dragAround = false;
        }

        if (dragAround)
        {
            //Sem Damping
            float currentAngle = transform.eulerAngles.y;
            float desiredAngle = target.transform.eulerAngles.y;
            float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * rotDamping);
            Quaternion rotation = Quaternion.Euler(0, angle, 0);

            transform.position = target.transform.position - (rotation * initialOffset);
        }
        else
        {
            //Com Damping
            float currentAngle = transform.eulerAngles.y;
            float desiredAngle = target.transform.eulerAngles.y;
            float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * rotDamping);
            Quaternion rotation = Quaternion.Euler(0, angle, 0);

            Vector3 desiredPosition = target.transform.position - (rotation * initialOffset);
            Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * distDamping);
            transform.position = position;
        }

        transform.LookAt(target.transform);
    }
}