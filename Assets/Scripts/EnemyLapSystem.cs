﻿using UnityEngine;

public class EnemyLapSystem : MonoBehaviour
{
    public int lap = 1;
    private int currentChk = 0;
    public GameObject[] aChks;

    public void Start()
    {
        lap = 1;
    }

    public void Update()
    {

    }

    public void OnTriggerEnter(Collider hit)
    {
        for (int totalChks = 0; totalChks <= aChks.Length; totalChks++)
        {
            if (hit.gameObject == aChks[currentChk])
            {
               currentChk++;
               if (currentChk == aChks.Length)
               {
                   currentChk = 0; 
                   lap++;
                   if (lap == 4)
                   {
                       GameObject.Find("PlayerKart").GetComponent<MoveRabit>().GameLost();
                   }
               }
            }
        }
    }
}